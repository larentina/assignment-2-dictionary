%define FIRST_EL 0
%macro colon 2
	%2: dq FIRST_EL
	    db %1, 0
	%define FIRST_EL %2
%endmacro