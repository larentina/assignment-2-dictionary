%include "lib.inc"
global find_word
%define DQ_SIZE 8

section .text

find_word:
	xor rax, rax
	push r12
	push r13
	mov r12, rdi		;в r12 - указатель на нуль-терминированную строку
	mov r13, rsi		;в r13 - указатель на начало словаря
.loop:
	test r13, r13		;проверка на достижение конца связного списка
	je .end_of_LL
	mov rdi, r12
	lea rsi, [r13+DQ_SIZE]	;в rsi - указатель на ключ
	call string_equals
	cmp rax, 1		;если string_equals вернула 1, то сроки равны
	jz .equals
	mov r13, [r13]		;в r13 - указатель на следующий узел
	jmp .loop
.equals:
	mov rax, r13
	jmp .out
.end_of_LL:
	xor rax, rax
.out:
	pop r13
	pop r12
	ret
	