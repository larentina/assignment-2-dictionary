%include "lib.inc"
%include "words.inc"
%include "dict.inc"
%define DQ_SIZE 8
%define BUFFER_SIZE 256
section .bss
buffer: resb BUFFER_SIZE 

section .rodata
buffer_overflow: db "Input data more than buffer size(255)", 0
key_not_found: db "The key is not found", 0

section .text
global _start
_start:
	mov rdi, buffer
	mov rsi, BUFFER_SIZE
	call read_line
	test rax, rax		;проверяем переполнен ли буффер
	jz .buffer_overflow_error
	mov rdi, rax		;в rdi-указатель на буффер
	mov rsi, FIRST_EL	;в rsi-указатель на список
	call find_word		;ищем такой же ключ в списке
	test rax, rax		;если его нет, то
	jz .key_not_found	;преходим на метку .key_not_found
	mov rdi, rax		;в rax адрес начала вхождения в список
        add rdi, DQ_SIZE		;в rdi адрес ключа
	push rdi
	call string_length	;в rax-длину ключа
	inc rax			;к длине ключа прибавляем нуль-терминатор
	pop rdi
	lea rdi, [rdi+rax]	;в rdi - указатель на значение ключа	
	call print_string
.out:
	call print_newline
	call exit
.buffer_overflow_error:
	mov rdi,buffer_overflow
	jmp .print_error
.key_not_found:
	mov rdi, key_not_found	
.print_error:
	call print_string
	jmp .out		

	
	