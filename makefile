ASM=nasm
ASMFLAGS=-f elf64
PYTHON = python2
LD = ld
.PHONY: clean test
%.o: %.asm 
	$(ASM) $(ASMFLAGS) $(ASMVERSION) -o $@ $< 
main.o: main.asm lib.inc words.inc dict.inc
	$(ASM) $(ASMFLAGS) $(ASMVERSION) -o $@ $< 

program: main.o lib.o dict.o
	$(LD) -o $@ $^
clean:
	rm -f *.o program
test:
	$(PYTHON) test.py
