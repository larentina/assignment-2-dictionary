import subprocess

test_data = [
    {"input_data": "first", "expected_output": "Vybiray ITMO\n"},
    {"input_data": "second", "expected_output": "i\n"},
    {"input_data": "third", "expected_output": "ne vybiray voobshche\n"},
    {"input_data": "sljdfhsdlvg", "expected_output":"The key is not found\n" },
    {"input_data": "Loem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum condimentum vehicula tellus non commodo. Quisque id velit sit amet dui luctus semper id at odio. Nullam at vulputate enim. Nunc gravida pretium ligula sed pellentesque. Fusce pulvinar sap", "expected_output":"The key is not found\n" },
    {"input_data": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum condimentum vehicula tellus non commodo. Quisque id velit sit amet dui luctus semper id at odio. Nullam at vulputate enim. Nunc gravida pretium ligula sed pellentesque. Fusce pulvinar sapI", "expected_output":"Input data more than buffer size(255)\n" }
]

for i in range(len(test_data)):
    process = subprocess.Popen(['./program'], stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)
 

    process.stdin.write(test_data[i]["input_data"] + '\n')
    process.stdin.flush()

    output, error = process.communicate()

    if output == test_data[i]["expected_output"]:
        print("Test {} passed".format(i))
    else:
        print("Test {} failed: unexpected output".format(i))
        print("Expected output:")
        print(test_data[i]["expected_output"])
        print("Received output:")
        print(output)
